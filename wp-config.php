<?php
/**
 * Il file base di configurazione di WordPress.
 *
 * Questo file viene utilizzato, durante l’installazione, dallo script
 * di creazione di wp-config.php. Non è necessario utilizzarlo solo via web
 * puoi copiare questo file in «wp-config.php» e riempire i valori corretti.
 *
 * Questo file definisce le seguenti configurazioni:
 *
 * * Impostazioni MySQL
 * * Chiavi Segrete
 * * Prefisso Tabella
 * * ABSPATH
 *
 * * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Impostazioni MySQL - È possibile ottenere queste informazioni dal proprio fornitore di hosting ** //
/** Il nome del database di WordPress */
define( 'DB_NAME', 'simonebasezzi' );

/** Nome utente del database MySQL */
define( 'DB_USER', 'root' );

/** Password del database MySQL */
define( 'DB_PASSWORD', '' );

/** Hostname MySQL  */
define( 'DB_HOST', 'localhost' );

/** Charset del Database da utilizzare nella creazione delle tabelle. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Il tipo di Collazione del Database. Da non modificare se non si ha idea di cosa sia. */
define('DB_COLLATE', '');

/**#@+
 * Chiavi Univoche di Autenticazione e di Salatura.
 *
 * Modificarle con frasi univoche differenti!
 * È possibile generare tali chiavi utilizzando {@link https://api.wordpress.org/secret-key/1.1/salt/ servizio di chiavi-segrete di WordPress.org}
 * È possibile cambiare queste chiavi in qualsiasi momento, per invalidare tuttii cookie esistenti. Ciò forzerà tutti gli utenti ad effettuare nuovamente il login.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '>bhzM?`UX:<689+Ed!nYdAO &O-[`3.>>|!daGi1w64dIq*,YI50W1W%Pd/fhLq<' );
define( 'SECURE_AUTH_KEY',  'r5Y[&j.&8[6D[hEU7Fy!H{a+oy<n0N9(F9do)o$U-QtU/u[wD:GF[EO)_sqTR|&d' );
define( 'LOGGED_IN_KEY',    ')>KqPS!iq]?,y`Au?kF[*XQk?N%AM9DxAlUSag#A=:>SN;xh!{,zx1?+-BH=Lu]r' );
define( 'NONCE_KEY',        '{w<)vB4E6lp}lcyj=~iA;lS)@718@0FF[!;1=2}A^c /!OB3yTb+F[|sk#(4:!-r' );
define( 'AUTH_SALT',        'j{L~8FP<W5tJhlb%~F}1l]jT% FVe |i5px#b0%Q-1ccL~-1.Q_6]CicQ8TzSWIi' );
define( 'SECURE_AUTH_SALT', '_M Ir]UJ_b2nul&rFtlB^j+$j9Q>pZ|TN/ww#bpO>/DUMF]&OM;:F;GWD/lI6JHL' );
define( 'LOGGED_IN_SALT',   'd0ZcqX`g/Q6k<.(irA_?nyzb^HK2B@*W!q)1&Ca#BBg8z+nnIRt;WD}LV4L2Xxw*' );
define( 'NONCE_SALT',       '`%%r%B=b9tTg:3vU266Hkb~KoTA2D$SM)S>w~$;v=8^)GGX4IxJKu1:A`4uMg^zv' );

/**#@-*/

/**
 * Prefisso Tabella del Database WordPress.
 *
 * È possibile avere installazioni multiple su di un unico database
 * fornendo a ciascuna installazione un prefisso univoco.
 * Solo numeri, lettere e sottolineatura!
 */
$table_prefix = 'wp_';

/**
 * Per gli sviluppatori: modalità di debug di WordPress.
 *
 * Modificare questa voce a TRUE per abilitare la visualizzazione degli avvisi durante lo sviluppo
 * È fortemente raccomandato agli svilupaptori di temi e plugin di utilizare
 * WP_DEBUG all’interno dei loro ambienti di sviluppo.
 *
 * Per informazioni sulle altre costanti che possono essere utilizzate per il debug,
 * leggi la documentazione
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define('WP_DEBUG', false);

/* Finito, interrompere le modifiche! Buon blogging. */

/** Path assoluto alla directory di WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Imposta le variabili di WordPress ed include i file. */
require_once(ABSPATH . 'wp-settings.php');
